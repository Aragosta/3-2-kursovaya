<?php
//Подключение шапки
require_once("../elements/base/header.php");
?>
<!-- Подключение Bootstrap CSS -->
<link rel="stylesheet" href="../bootstrap-5.0.1-dist/bootstrap-5.0.1-dist/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="../../static/css/styles.css">
<!-- Блок для вывода сообщений -->
<div class="block_for_messages">
    <?php
    $returnedName = '';
    if (isset($_SESSION["returnedName"]) && !empty($_SESSION["returnedName"])) {
        $returnedName = $_SESSION["returnedName"];
        unset($_SESSION["returnedName"]);
    }
    $returnedSurname = '';
    if (isset($_SESSION["returnedSurname"]) && !empty($_SESSION["returnedSurname"])) {
        $returnedSurname = $_SESSION["returnedSurname"];
        unset($_SESSION["returnedSurname"]);
    }
    $returnedPhone = '';
    if (isset($_SESSION["returnedPhone"]) && !empty($_SESSION["returnedPhone"])) {
        $returnedPhone = $_SESSION["returnedPhone"];
        unset($_SESSION["returnedPhone"]);
    }
    $returnedEmail = '';
    if (isset($_SESSION["returnedEmail"]) && !empty($_SESSION["error_messages"])) {
        $returnedEmail = $_SESSION["returnedEmail"];
        unset($_SESSION["returnedEmail"]);
    }
    // Если в сессии существуют сообщения об ошибках, то выводим их
    if (isset($_SESSION["error_messages"]) && !empty($_SESSION["error_messages"])) {
        echo $_SESSION["error_messages"];

        // Уничтожаем чтобы не выводились заново при обновлении страницы
        unset($_SESSION["error_messages"]);
    }

    // Если в сессии существуют радостные сообщения, то выводим их
    if (isset($_SESSION["success_messages"]) && !empty($_SESSION["success_messages"])) {
        echo $_SESSION["success_messages"];

        // Уничтожаем чтобы не выводились заново при обновлении страницы
        unset($_SESSION["success_messages"]);
    }
    ?>
</div>

<?php
// Проверяем, если пользователь не авторизован, то выводим форму регистрации,
// иначе выводим сообщение о том, что он уже зарегистрирован
if (!isset($_SESSION["email"]) && !isset($_SESSION["password"])) {
?>

    <div id="form_register">
        <h2 class="text-center" style="margin-top: 30px;">Регистрация</h2>

        <form action="../handlers/register.php" method="post" name="form_register">
            <!--Имя-->
            <div>
                <div class="input-group input-group-sm mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-sm">Имя</span>
                    </div>
                    <input type="text" class="form-control" name="first_name" value="<?= $returnedName ?>" aria-label="Small" maxlength="50" autocomplete="off" required="required" aria-describedby="inputGroup-sizing-sm">
                </div>
                <div><span id="valid_first_name_message" class="mesage_error mb-1"></span></div>
            </div>
            <!--Фамилия-->
            <div>
                <div class="input-group input-group-sm mt-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-sm">Фамилия</span>
                    </div>
                    <input type="text" class="form-control" name="last_name" value="<?= $returnedSurname ?>" aria-label="Small" maxlength="50" autocomplete="off" required="required" aria-describedby="inputGroup-sizing-sm">
                </div>
                <div><span id="valid_last_name_message" class="mesage_error mb-1"></span></div>
            </div>
            <!--Телефон-->
            <div>
                <div class="input-group input-group-sm mt-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-sm">Телефон</span>
                    </div>
                    <input type="text" class="form-control tel" name="phone" value="<?= $returnedPhone ?>" aria-label="Small" maxlength="17" autocomplete="off" required="required" aria-describedby="inputGroup-sizing-sm">
                </div>
                <div><span id="valid_phone_message" class="mesage_error mb-1"></span></div>
            </div>
            <!--Email-->
            <div>
                <div class="input-group input-group-sm mt-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-sm">Email</span>
                    </div>
                    <input type="email" class="form-control" name="email" value="<?= $returnedEmail ?>" aria-label="Small" maxlength="255" autocomplete="off" required="required" aria-describedby="inputGroup-sizing-sm">
                </div>
                <div><span id="valid_email_message" class="mesage_error mb-1"></span></div>
            </div>
            <!--Пароль-->
            <div>
                <div class="input-group input-group-sm mt-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-sm">Пароль</span>
                    </div>
                    <input type="password" class="form-control" name="password" placeholder="минимум 6 символов" aria-label="Small" maxlength="50" required="required" aria-describedby="inputGroup-sizing-sm">
                </div>
                <div><span id="valid_password_message" class="mesage_error mb-1"></span></div>
            </div>
            <!--Подтверждение пароля-->
            <div>
                <div class="input-group input-group-sm mt-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-sm">Подтверждение пароля</span>
                    </div>
                    <input type="password" class="form-control" name="password_check" placeholder="минимум 6 символов" aria-label="Small" maxlength="50" required="required" aria-describedby="inputGroup-sizing-sm">
                </div>
                <div><span id="valid_password_check_message" class="mesage_error mb-1"></span></div>
            </div>

            <input type="submit" name="btn_submit_register" class="btn btn-primary mt-3" style="margin: 0 auto;" value="Зарегистрироваться">
        </form>
        <h3 class="text-center" style="margin-top: 30px;">Уже зарегистрированы?</h3>
        <a class="btn btn-primary" style="width: 179px; margin: 0 auto;" href="/elements/form_auth.php">Авторизоваться</a>
        <!-- Подключение Bootstrap JavaScript с Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    </div>
<?php
} else {
?>
    <div id="authorized">
        <h2>Вы уже зарегистрированы</h2>
    </div>
<?php
}

// Подключение подвала
require_once("../elements/base/footer.php");
?>