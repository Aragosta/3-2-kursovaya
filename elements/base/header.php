<?php
// Устанавливаем время жизни куки 2 недели
session_set_cookie_params(2 * 7 * 24 * 60 * 60);
// Запускаем сессию
session_start();
if (!isset($_SESSION["current_page"])) {
    $_SESSION["current_page"] = "index";
}

$url = $_SERVER['REQUEST_URI'];
$url = explode('?', $url);
$url = $url[0];

?>

<!DOCTYPE html>
<html>

<head>
    <title>MedicStuff</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../bootstrap-5.0.1-dist/bootstrap-5.0.1-dist/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../static/css/styles.css">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <!-- Сниппет -->
    <meta name="description" content="Большой выбор антисептиков, медицинских масок и других товаров для защиты Вашего здоровья. Гарантия от производителя, удобная система заказа">
    <!-- Чат-бот -->
    <link rel="stylesheet" href="../../static/css/chatbot.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <script type="text/javascript">
        // Валидация на клиенте
        $(document).ready(function () {
            "use strict";
            // ПРОВЕРКА ИМЕНИ
            var first_name = $('input[name=first_name]');
            first_name.blur(function () {
                if (first_name.val() !== '') {
                    let first_name_len = first_name.val().length;
                    // Если длина введенного имени больше 50 символов, то выводим сообщение об ошибке
                    if (first_name_len > 50) {
                        // Выводим сообщение об ошибке
                        $('#valid_first_name_message').text('Максимальная длина имени составляет 50 символов');
                        // Дезактивируем кнопку отправки
                        $('input[type=submit]').attr('disabled', true);
                    } else if (first_name_len < 2) {
                        // Выводим сообщение об ошибке
                        $('#valid_first_name_message').text('Минимальная длина имени составляет 2 символа');
                        // Дезактивируем кнопку отправки
                        $('input[type=submit]').attr('disabled', true);
                    } else {
                        // Убираем сообщение об ошибке
                        $('#valid_first_name_message').text('');
                        // Активируем кнопку отправки
                        $('input[type=submit]').attr('disabled', false);
                    }
                } else {
                    $('#valid_first_name_message').text('Введите имя');
                    // Дезактивируем кнопку отправки
                    $('input[type=submit]').attr('disabled', true);
                }
            });

            // ПРОВЕРКА ФАМИЛИИ
            var last_name = $('input[name=last_name]');
            last_name.blur(function () {
                if (last_name.val() !== '') {
                    let last_name_len = last_name.val().length;
                    // Если длина введенной фамилии больше 50 символов, то выводим сообщение об ошибке
                    if (last_name_len > 50) {
                        // Выводим сообщение об ошибке
                        $('#valid_last_name_message').text('Максимальная длина фамилии составляет 50 символов');
                        // Дезактивируем кнопку отправки
                        $('input[type=submit]').attr('disabled', true);
                    } else if (last_name_len < 2) {
                        // Выводим сообщение об ошибке
                        $('#valid_last_name_message').text('Минимальная длина фамилии составляет 2 символа');
                        // Дезактивируем кнопку отправки
                        $('input[type=submit]').attr('disabled', true);
                    } else {
                        // Убираем сообщение об ошибке
                        $('#valid_last_name_message').text('');
                        // Активируем кнопку отправки
                        $('input[type=submit]').attr('disabled', false);
                    }
                } else {
                    $('#valid_last_name_message').text('Введите фамилию');
                    // Дезактивируем кнопку отправки
                    $('input[type=submit]').attr('disabled', true);
                }
            });

            // ПРОВЕРКА ТЕЛЕФОНА
            var phone = $('input[name=phone]');
            phone.blur(function () {
                if (phone.val() !== '') {
                    let phone_len = phone.val().length;
                    // Если длина введенного телефона не равна 17 символам, то выводим сообщение об ошибке
                    if (phone_len < 17) {
                        // Выводим сообщение об ошибке
                        $('#valid_phone_message').text('Некорректный номер телефона');
                        // Дезактивируем кнопку отправки
                        $('input[type=submit]').attr('disabled', true);
                    } else if (phone_len > 17) {
                        // Выводим сообщение об ошибке
                        $('#valid_phone_message').text('Максимальная длина номера телефона составляет 17 символов');
                        // Дезактивируем кнопку отправки
                        $('input[type=submit]').attr('disabled', true);
                    } else {
                        // Убираем сообщение об ошибке
                        $('#valid_phone_message').text('');
                        // Активируем кнопку отправки
                        $('input[type=submit]').attr('disabled', false);
                    }
                } else {
                    $('#valid_phone_message').text('Введите номер телефона');
                    // Дезактивируем кнопку отправки
                    $('input[type=submit]').attr('disabled', true);
                }
            });

            // ПРОВЕРКА EMAIL
            // регулярное выражение для проверки email
            var pattern = /^[a-z0-9][a-z0-9\._-]*[a-z0-9]*@([a-z0-9]+([a-z0-9-]*[a-z0-9]+)*\.)+[a-z]{2,4}/i;
            var mail = $('input[name=email]');
            mail.blur(function () {
                if (mail.val() !== '') {
                    let mail_len = mail.val().length;
                    // Проверяем, если введенный email соответствует регулярному выражению
                    if (mail.val().search(pattern) === 0) {
                        // Убираем сообщение об ошибке
                        $('#valid_email_message').text('');
                        // Активируем кнопку отправки
                        $('input[type=submit]').attr('disabled', false);
                        // Проверка максимальной длины email
                        if (mail_len > 255) {
                            //Выводим сообщение об ошибке
                            $('#valid_email_message').text('Максимальная длина email составляет 255 символов');
                            // Дезактивируем кнопку отправки
                            $('input[type=submit]').attr('disabled', true);
                        } else {
                            // Убираем сообщение об ошибке
                            $('#valid_email_message').text('');
                            // Активируем кнопку отправки
                            $('input[type=submit]').attr('disabled', false);
                        }
                    } else {
                        // Выводим сообщение об ошибке
                        $('#valid_email_message').text('Неправильный формат email (пример: test@test.com)');
                        // Дезактивируем кнопку отправки
                        $('input[type=submit]').attr('disabled', true);
                    }
                } else {
                    $('#valid_email_message').text('Введите email');
                    // Дезактивируем кнопку отправки
                    $('input[type=submit]').attr('disabled', true);
                }
            });

            // ПРОВЕРКА ПАРОЛЯ
            var password = $('input[name=password]');
            password.blur(function () {
                if (password.val() !== '') {
                    let password_len = password.val().length;
                    // Если длина введенного пароля меньше 6 или больше 50 символов, то выводим сообщение об ошибке
                    if (password_len < 6) {
                        // Выводим сообщение об ошибке
                        $('#valid_password_message').text('Минимальная длина пароля составляет 6 символов');
                        // Дезактивируем кнопку отправки
                        $('input[type=submit]').attr('disabled', true);
                    } else if (password_len > 50) {
                        // Выводим сообщение об ошибке
                        $('#valid_password_message').text('Максимальная длина пароля составляет 50 символов');
                        // Дезактивируем кнопку отправки
                        $('input[type=submit]').attr('disabled', true);
                    } else {
                        let password_val = password.val();
                        if (password_val.search(/[a-z]/i) < 0) {
                            $('#valid_password_message').text('Пароль должен содержать как минимум 1 букву');
                            $('input[type=submit]').attr('disabled', true);
                        } else if (password_val.search(/[0-9]/i) < 0) {
                            $('#valid_password_message').text('Пароль должен содержать как минимум 1 цифру');
                            $('input[type=submit]').attr('disabled', true);
                        } else if (password_val.search(/[!@#$%^&*]/i) < 0) {
                            $('#valid_password_message').text('Пароль должен содержать как минимум 1 спецсимвол');
                            $('input[type=submit]').attr('disabled', true);
                        } else {
                            // Убираем сообщение об ошибке
                            $('#valid_password_message').text('');
                            // Активируем кнопку отправки
                            $('input[type=submit]').attr('disabled', false);
                        }
                    }
                } else {
                    $('#valid_password_message').text('Введите пароль');
                    // Дезактивируем кнопку отправки
                    $('input[type=submit]').attr('disabled', true);
                }
            });

            // ПРОВЕРКА ПАРОЛЯ-ПОДТВЕРЖДЕНИЯ
            var password_check = $('input[name=password_check]');
            password_check.blur(function () {
                if (password_check.val() !== '') {
                    // Если пароли не совпадают, то выводим сообщение об ошибке
                    if (password.val() !== password_check.val()) {
                        // Выводим сообщение об ошибке
                        $('#valid_password_check_message').text('Введенные пароли не совпадают');
                        // Дезактивируем кнопку отправки
                        $('input[type=submit]').attr('disabled', true);
                    } else {
                        // Убираем сообщение об ошибке
                        $('#valid_password_check_message').text('');
                        // Активируем кнопку отправки
                        $('input[type=submit]').attr('disabled', false);
                    }
                } else {
                    $('#valid_password_check_message').text('Введите пароль-подтверждение');
                    // Дезактивируем кнопку отправки
                    $('input[type=submit]').attr('disabled', true);
                }
            });
        });

        window.addEventListener("DOMContentLoaded", function() {
            [].forEach.call( document.querySelectorAll('.tel'), function(input) {
                var keyCode;
                function mask(event) {
                    event.keyCode && (keyCode = event.keyCode);
                    var pos = this.selectionStart;
                    if (pos < 3) event.preventDefault();
                    var matrix = "+7 (___) ___ ____",
                        i = 0,
                        def = matrix.replace(/\D/g, ""),
                        val = this.value.replace(/\D/g, ""),
                        new_value = matrix.replace(/[_\d]/g, function(a) {
                            return i < val.length ? val.charAt(i++) || def.charAt(i) : a
                        });
                    i = new_value.indexOf("_");
                    if (i != -1) {
                        i < 5 && (i = 3);
                        new_value = new_value.slice(0, i)
                    }
                    var reg = matrix.substr(0, this.value.length).replace(/_+/g,
                        function(a) {
                            return "\\d{1," + a.length + "}"
                        }).replace(/[+()]/g, "\\$&");
                    reg = new RegExp("^" + reg + "$");
                    if (!reg.test(this.value) || this.value.length < 5 || keyCode > 47 && keyCode < 58) this.value = new_value;
                    if (event.type == "blur" && this.value.length < 5)  this.value = ""
                }

                input.addEventListener("input", mask, false);
                input.addEventListener("focus", mask, false);
                input.addEventListener("blur", mask, false);
                input.addEventListener("keydown", mask, false)
            });
        });
    </script>
</head>
<body>
<div class="fixed-top">
    <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #36AFA8;">
        <div class="container-fluid">
            <button class="navbar-toggle navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false"
                    aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="container">
                    <div class="row">
                        <div class="navbar-nav">
                            <a href="/index.php" class="btn btn-primary <?php if ($url == "/index.php" || $url == "/") { echo "active"; } ?>" aria-current="page">КАТАЛОГ</a>
                            <a href="#footer" class="btn btn-primary">КОНТАКТЫ</a>
                            <?php
                            // Проверяем авторизован ли пользователь
                            if (!isset($_SESSION['email']) && !isset($_SESSION['password'])) {
                                // если нет, то выводим блок с ссылками на страницу регистрации и авторизации
                                ?>
                                <a href="/elements/form_auth.php" class="btn btn-primary <?php if ($url == "/elements/form_auth.php" || $url == "/elements/form_register.php") { echo "active"; } ?>">ВОЙТИ</a>
                                <?php
                            } else {
                                // Если пользователь авторизован, то выводим ссылку Выход
                                ?>
                                <a href="../home.php" class="btn btn-primary <?php if ($url == "/home.php" || $url == "/articles.php" || $url == "/orders.php") { echo "active"; } ?>" aria-current="page">ЛИЧНЫЙ КАБИНЕТ</a>
                                <a href="/handlers/logout.php" class="btn btn-primary" aria-current="page">ВЫХОД</a>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <div class="rectangle_header"></div>
</div>
<div style="height: 90px"></div>