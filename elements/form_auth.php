<?php
//Подключение шапки
require_once("../elements/base/header.php");
?>
<!-- Подключение Bootstrap CSS -->
<link rel="stylesheet" href="../bootstrap-5.0.1-dist/bootstrap-5.0.1-dist/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="../../static/css/styles.css">
<!-- Блок для вывода сообщений -->
<div class="block_for_messages">
<?php
$returnedEmail = '';
if (isset($_SESSION["returnedEmail"]) && !empty($_SESSION["returnedEmail"])) {
    $returnedEmail = $_SESSION["returnedEmail"];
    unset($_SESSION["returnedEmail"]);
}

if (isset($_SESSION["error_messages"]) && !empty($_SESSION["error_messages"])) {
    echo $_SESSION["error_messages"];

    //Уничтожаем чтобы не появилось заново при обновлении страницы
    unset($_SESSION["error_messages"]);
}

if (isset($_SESSION["success_messages"]) && !empty($_SESSION["success_messages"])) {
    echo $_SESSION["success_messages"];

    //Уничтожаем чтобы не появилось заново при обновлении страницы
    unset($_SESSION["success_messages"]);
}
?>
</div>
<?php
//Проверяем, если пользователь не авторизован, то выводим форму авторизации, 
//иначе выводим сообщение о том, что он уже авторизован
if (!isset($_SESSION["email"]) && !isset($_SESSION["password"])) {
    //border: 4px double red; todo при ошибке рисовать рамку
?>
    <div id="form_auth">
        <h2 class="text-center" style="margin-top: 30px;">Авторизация</h2>
        <form action="../handlers/auth.php" method="post" name="form_auth">
            <div>
                <div class="input-group input-group-sm mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-sm">Email</span>
                    </div>
                    <input type="email" class="form-control" name="email" value="<?= $returnedEmail ?>" aria-label="Small" maxlength="255" autocomplete="off" required="required" aria-describedby="inputGroup-sizing-sm">
                </div>
                <div><span id="valid_email_message" class="mesage_error mb-1"></span></div>
            </div>

            <div>
                <div class="input-group input-group-sm mt-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-sm">Пароль</span>
                    </div>
                    <input type="password" class="form-control" name="password" placeholder="минимум 6 символов" aria-label="Small" maxlength="50" required="required" aria-describedby="inputGroup-sizing-sm">
                </div>
                <div><span id="valid_password_message" class="mesage_error mb-1"></span></div>
            </div>

            <input type="submit" name="btn_submit_auth" class="btn btn-primary mt-3" style="margin: 0 auto;" value="Авторизоваться">
        </form>
            <h3 class="text-center" style="margin-top: 30px;">Ещё не имеете аккаунт?</h3>
    <!--        <div>-->
            <a class="btn btn-primary" style="width: 210px; margin: 0 auto;" href="/elements/form_register.php">Зарегистрироваться</a>
<!--        </div>-->
        <!-- Подключение Bootstrap JavaScript с Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    </div>
<?php
} else {
?>
<div id="authorized">
    <h2>Вы уже авторизованы</h2>
</div>
<?php
}
?>
<?php
//Подключение подвала
require_once("../elements/base/footer.php");
?>