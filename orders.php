<?php
//Подключение шапки
require_once("elements/base/header.php");
?>
<!-- Подключение Bootstrap CSS -->
<!--<link rel="stylesheet" href="../bootstrap-5.0.1-dist/bootstrap-5.0.1-dist/css/bootstrap.css">-->
<!--<link rel="stylesheet" type="text/css" href="../../static/css/styles.css">-->
<!--<script src="static/js/jquery.min.js"></script>-->
<!-- Блок для вывода сообщений -->
<div class="block_for_messages">
    <?php
    // Если в сессии существуют сообщения об ошибках, то выводим их
    if (isset($_SESSION["error_messages"]) && !empty($_SESSION["error_messages"])) {
        echo $_SESSION["error_messages"];

        // Уничтожаем чтобы не выводились заново при обновлении страницы
        unset($_SESSION["error_messages"]);
    }

    // Если в сессии существуют радостные сообщения, то выводим их
    if (isset($_SESSION["success_messages"]) && !empty($_SESSION["success_messages"])) {
        echo $_SESSION["success_messages"];

        // Уничтожаем чтобы не выводились заново при обновлении страницы
        unset($_SESSION["success_messages"]);
    }
    ?>
</div>

<?php
// Проверяем, если пользователь не авторизован, то выводим форму регистрации,
// иначе выводим сообщение о том, что он уже зарегистрирован
if (isset($_SESSION["email"]) && isset($_SESSION["password"])) {
//todo реализовать проверку роли пользователя
//todo фильтр: все, по статусу (несколько кнопок)
    // данные подключения к базе данных
    $server = "localhost";
    $username = "root";
    $password = "";
    $database = "web-kursovaya";

    // Подключение к базе данный
    $mysqli = new mysqli($server, $username, $password, $database);

    // Проверка успешности соединения
    if ($mysqli->connect_errno) {
        die("<p><strong>Ошибка подключения к БД</strong></p><p><strong>Код ошибки: </strong> " . $mysqli->connect_errno . " </p><p><strong>Описание ошибки:</strong> " . $mysqli->connect_error . "</p>");
    }

    // Устанавливаем кодировку подключения
    $mysqli->set_charset('utf8');

    $counter = 0;
    if (isset($_POST['updateOrder'])) {
        $cnt = 0;
        // получаем список товаров
        $query_all_products = $mysqli->query("SELECT * FROM `orders`");
        foreach ($query_all_products as $row_pr) {
            $cnt += 1;
            $name = "select" . $cnt;
            if (isset($_POST[$name])) {
                $id = $_POST[$name];
                $result_query_insert = $mysqli->query("UPDATE `orders` SET status = '" . $id . "' WHERE id_order = '" . $row_pr["id_order"] . "'");
            }
        }
    }
    $email = $_SESSION['email'];
    $result_query_select = $mysqli->query("SELECT * FROM `users` WHERE email = '" . $email . "'");
    $role = 1;
    foreach ($result_query_select as $row) {
        $role = $row["role"];
    }
    if ($role == 2) {
    ?>
    <h2 class="text-center"><a href="/home.php" style="text-decoration: none;">←</a>   Заявки</h2>
    <form method="post">
        <div class="form-group" style="width: 440px; margin: 0 auto;">
            <input type="submit" name="allOrders" value="Все заявки">
            <input type="submit" name="onConsideration" value="На рассмотрении">
            <input type="submit" name="approved" value="Одобрена">
            <input type="submit" name="rejected" value="Отклонена">
        </div>
    </form>

    <div class="order__container">
        <div class="order__block text-center text-break">
            <div class="order__fio">
                Клиент
            </div>
            <div class="order__contacts">
                Контактные данные
            </div>
            <div class="order__product">
                Артикул товара
            </div>
            <div class="order__status">
                Статус
            </div>
        </div>
        <hr>
        <?php
        $query_string = "";
        if (isset($_POST['allOrders'])) {
            $query_string = "SELECT * FROM `orders`";
        }

        if (isset($_POST['onConsideration'])) {
            $query_string = "SELECT * FROM `orders` WHERE `orders`.`status` = 1";
        }

        if (isset($_POST['approved'])) {
            $query_string = "SELECT * FROM `orders` WHERE `orders`.`status` = 2";
        }

        if (isset($_POST['rejected'])) {
            $query_string = "SELECT * FROM `orders` WHERE `orders`.`status` = 3";
        }

        if (!isset($_POST['allOrders']) && !isset($_POST['onConsideration']) && !isset($_POST['approved']) && !isset($_POST['rejected'])) {

            $query_string = "SELECT * FROM `orders`";
        }

        // получаем список заявок
        $query_products = $mysqli->query($query_string);
        foreach ($query_products as $row_pr) {
            echo '<div class="order__block text-center text-break">';
            $counter = $counter + 1;
            // получаем имя и контактные данные клиента
            $query_string = "SELECT name, surname, phone, email FROM `users` join `orders` WHERE `orders`.`id_user` = `users`.`id_user` AND `orders`.`id_order` = " . $row_pr["id_order"];
            $query_user = $mysqli->query($query_string);

            foreach ($query_user as $row_user) {
                echo '<div class="order__fio">';
                echo $row_user["name"] . " " . $row_user["surname"];
                echo '</div>';

                echo '<div class="order__contacts">';
                echo $row_user["email"] . " " . $row_user["phone"];
                echo '</div>';
            }

            echo '<div class="order__product">';
            echo $row_pr["id_product"] . " " . $row_pr["name"];
            echo '</div>';

            echo '<div class="order__status">';
            $status = $row_pr["status"];
            $selected1 = "";
            $selected2 = "";
            $selected3 = "";
            if ($status == 1) {
                $selected1 = "selected "; // На рассмотрении
            } else if ($status == 2) {
                // дальнейшее сотрудничество (за рамками курсовой)
                $selected2 = "selected "; // Одобрена
            } else if ($status == 3) {
                // отказ в сотрудничестве из-за клиента
                $selected3 = "selected "; // Отклонена
            }
            echo '<form method="post">';
            echo '<select name="select' . $counter . '">';
            echo '<option ' . $selected1 . 'value="1">На рассмотрении</option>';
            echo '<option ' . $selected2 . 'value="2">Одобрена</option>';
            echo '<option ' . $selected3 . 'value="3">Отклонена</option>';
            echo '</select>';
            echo '<input type="submit" name="updateOrder" value="✅">';
            echo '</form>';
            echo '</div>';

            echo '</div>';
        }
        ?>
    </div>
        <?php
    } else {
        ?>
        <div id="authorized">
            <h2>Ошибка: недостаточно прав для просмотра содержимого</h2>
        </div>
        <?php
    }
    ?>
    <?php
} else {
    ?>
    <div id="authorized">
        <h2>Авторизуйтесь</h2>
    </div>
    <?php
}

// Подключение подвала
require_once("elements/base/footer.php");
?>
