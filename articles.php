<?php
//Подключение шапки
require_once("elements/base/header.php");
?>
    <!-- Подключение Bootstrap CSS -->
    <link rel="stylesheet" href="../bootstrap-5.0.1-dist/bootstrap-5.0.1-dist/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../static/css/styles.css">
    <script src="static/js/jquery.min.js"></script>
    <!-- Блок для вывода сообщений -->
    <div class="block_for_messages">
        <?php
        // Если в сессии существуют сообщения об ошибках, то выводим их
        if (isset($_SESSION["error_messages"]) && !empty($_SESSION["error_messages"])) {
            echo $_SESSION["error_messages"];

            // Уничтожаем чтобы не выводились заново при обновлении страницы
            unset($_SESSION["error_messages"]);
        }

        // Если в сессии существуют радостные сообщения, то выводим их
        if (isset($_SESSION["success_messages"]) && !empty($_SESSION["success_messages"])) {
            echo $_SESSION["success_messages"];

            // Уничтожаем чтобы не выводились заново при обновлении страницы
            unset($_SESSION["success_messages"]);
        }
        ?>
    </div>

<?php
// Проверяем, если пользователь не авторизован, то выводим форму регистрации,
// иначе выводим сообщение о том, что он уже зарегистрирован
if (isset($_SESSION["email"]) && isset($_SESSION["password"])) {
//todo реализовать проверку роли пользователя
//todo поиск: название товара + кнопка "Найти"
    // данные подключения к базе данных
    $server = "localhost";
    $username = "root";
    $password = "";
    $database = "web-kursovaya";

    // Подключение к базе данный
    $mysqli = new mysqli($server, $username, $password, $database);

    // Проверка успешности соединения
    if ($mysqli->connect_errno) {
        die("<p><strong>Ошибка подключения к БД</strong></p><p><strong>Код ошибки: </strong> " . $mysqli->connect_errno . " </p><p><strong>Описание ошибки:</strong> " . $mysqli->connect_error . "</p>");
    }

    // Устанавливаем кодировку подключения
    $mysqli->set_charset('utf8');

    $email = $_SESSION['email'];
    $result_query_select = $mysqli->query("SELECT * FROM `users` WHERE email = '" . $email . "'");
    $role = 1;
    foreach ($result_query_select as $row) {
        $role = $row["role"];
    }
    if ($role == 2) {
    ?>
    <h2 class="text-center"><a href="/home.php" style="text-decoration: none;">←</a>   Артикулы</h2>
    <form method="post">
        <div class="form-group" style="width: 440px; margin: 0 auto;">
            <label for="productsearch">Поиск наименования в базе:</label>
            <input autocomplete="off" class="form-control" id="productsearch" style="width: 440px" placeholder="Введите название" name="productsearch">
            <div style="width: 200px; margin: 0 auto;">
                <input type="submit" name="submit" value="Поиск">
                <input type="submit" name="submit1" id="submit1" value="Все результаты">
            </div>
        </div>
    </form>
    <div class="article__container">
        <div class="article__block">
            <div class="article__id text-center">
                Артикул
            </div>
            <div class="article__name text-end">
                Наименование товара
            </div>
            <div class="article__description text-center">
                Описание
            </div>
            <div class="article__cost_min text-center">
                Стоимость, ₽
            </div>
        </div>
        <hr>
        <?php
        // Поиск наименования в базе данных
        $product_search = $_POST['productsearch'];
        if (!empty($product_search)) {
            $query_productsearch = "SELECT * FROM `products` WHERE name LIKE '%$product_search%'";
            $query_all_products = $mysqli->query($query_productsearch);
            foreach ($query_all_products as $row_pr) {
                echo '<div class="article__block">';

                echo '<div class="article__id text-center">';
                echo $row_pr["id_product"];
                echo '</div>';

                echo '<div class="article__name">';
                echo $row_pr["name"];
                echo '</div>';

                echo '<div class="article__description">';
                echo $row_pr["description"];
                echo '</div>';

                echo '<div class="article__cost_min text-center">';
                echo $row_pr["cost_min"];
                echo '</div>';

                echo '</div>';
            }
        }

        if (isset($_POST['submit1'])) {
            // получаем список товаров
            $query_all_products = $mysqli->query("SELECT * FROM `products`");

            foreach ($query_all_products as $row_pr) {
                echo '<div class="article__block">';

                echo '<div class="article__id text-center">';
                echo $row_pr["id_product"];
                echo '</div>';

                echo '<div class="article__name">';
                echo $row_pr["name"];
                echo '</div>';

                echo '<div class="article__description">';
                echo $row_pr["description"];
                echo '</div>';

                echo '<div class="article__cost_min text-center">';
                echo $row_pr["cost_min"];
                echo '</div>';

                echo '</div>';
            }
        }

        if (!isset($_POST['submit1']) && !isset($_POST['productsearch'])) {
            // получаем список товаров
            $query_all_products = $mysqli->query("SELECT * FROM `products`");

            foreach ($query_all_products as $row_pr) {
                echo '<div class="article__block">';

                echo '<div class="article__id text-center">';
                echo $row_pr["id_product"];
                echo '</div>';

                echo '<div class="article__name">';
                echo $row_pr["name"];
                echo '</div>';

                echo '<div class="article__description">';
                echo $row_pr["description"];
                echo '</div>';

                echo '<div class="article__cost_min text-center">';
                echo $row_pr["cost_min"];
                echo '</div>';

                echo '</div>';
            }
        }
        ?>
    </div>
        <?php
    } else {
        ?>
    <div id="authorized">
        <h2>Ошибка: недостаточно прав для просмотра содержимого</h2>
    </div>
    <?php
        }
     ?>
    <?php
} else {
    ?>
    <div id="authorized">
        <h2>Авторизуйтесь</h2>
    </div>
    <?php
}

// Подключение подвала
require_once("elements/base/footer.php");
?>
