<?php

$path = __DIR__ . '/chats/';

$data['result'] = 'success';

// получаем данные, которые пришли на сервер
$input = file_get_contents('php://input');
// декодируем полученную JSON строку
$data = json_decode($input, true);
// проверяем была ли ошибка при декодировании JSON
if (json_last_error() !== JSON_ERROR_NONE) {
    $data['result'] = 'error';
    $data['error'] = 'Произошла ошибка при декодировании JSON строки';
    echo json_encode($data);
    exit();
}

// получаем id клиента
$idClient = $data['id'];
// получаем сообщения из чата
$chat = $data['chat'];

$start = $data['start'];
//
$date = $data['date'];

// имя файла
$fileName = $path . $idClient;

$output = '';

foreach ($chat as $key => $value) {
    $output .= $key . '[' . $value['type'] . ']:' . PHP_EOL;
    $output .= strip_tags($value['content']) . PHP_EOL;
    session_start();
    if (isset($_SESSION['email'])) {
        if (strip_tags($value['content']) == 'Отлично! Для этого введите идентификатор понравившегося товара ниже') {
            // Открытие новой сессии
            session_start();
            $_SESSION["user_start_choice"] = 'yes';
            $_SESSION["user_choice"] = 'yes';
        }
        else if (isset($_SESSION["user_choice"]) && $_SESSION["user_choice"] == 'yes') {
            if (strip_tags($value['content']) != 'Привет!' &&
                strip_tags($value['content']) != 'Я Арагоста - чат-бот этого сайта' &&
                strip_tags($value['content']) != 'Что Вас интересует?' &&
                strip_tags($value['content']) != 'Для этого перейдите в каталог товаров, откройте карточку понравившегося товара и запомните его идентификатор. Затем, если вы авторизованы, выберете в чат-боте опцию «Хочу оформить заказ».' &&
                strip_tags($value['content']) != 'Отлично! Для этого введите идентификатор понравившегося товара ниже' &&
                strip_tags($value['content']) != 'Мы записали вашу заявку! В ближайшее время Вам на почту придет сообщение от сотрудника магазина. С ним вы можете обсудить детали вашего заказа!' &&
                strip_tags($value['content']) != 'Привет! Я рад с тобой познакомиться' &&
                strip_tags($value['content']) != 'Салют!' &&
                strip_tags($value['content']) != 'Здравствуйте' &&
                strip_tags($value['content']) != 'Меня интересует, как я могу оформить заказ' &&
                strip_tags($value['content']) != 'Хочу оформить заказ' &&
                strip_tags($value['content']) != 'Меня интересует, как я могу поменять данные от учетной записи' &&
                strip_tags($value['content']) != "Для этого перейдите в личный кабинет, введите новые имя, фамилию и пароль и нажмите кнопку 'Поменять данные'" &&
                strip_tags($value['content']) != 'В начало') {

                $_SESSION["user_choice"] = '';

                $server = "localhost";
                $username = "root";
                $password = "";
                $database = "web-kursovaya";

                // Подключение к базе данных
                $mysqli = new mysqli($server, $username, $password, $database);

                $id_product = (int)strip_tags($value['content']);
                $checkAvailable = false;
                $checkFirstProduct = false;
                $firstIdProduct = 1;
                $query_all_products = $mysqli->query("SELECT * FROM `products`");
                foreach ($query_all_products as $row_pr) {
                    if (!$checkFirstProduct) {
                        $checkFirstProduct = true;
                        $firstIdProduct = $row_pr["id_product"];
                    }
                    if ($id_product == $row_pr["id_product"]) {
                        $checkAvailable = true;
                        break;
                    }
                }
                if (!$checkAvailable) {
                    $id_product = $firstIdProduct;
                }

                $result_query_select = $mysqli->query("SELECT * FROM `users` WHERE email = '" . $_SESSION['email'] . "'");
                $id_user = 0;

                foreach ($result_query_select as $row) {
                    $id_user = $row["id_user"];
                }

                // Устанавливаем кодировку подключения
                $mysqli->set_charset('utf8');

                // Запрос на добавление пользователя в БД
                $result_query_insert = $mysqli->query("INSERT INTO `orders` (id_product, id_user) VALUES ('" . $id_product . "', '" . $id_user . "') ");
            }
        }
    }
}

if (!file_exists($fileName)) {
    $text = '// ' . $idClient . ' //' . PHP_EOL . PHP_EOL;
    if ($start) {
        $text .= '/////// start ///////' . PHP_EOL . $date . PHP_EOL . '/////// start ///////' . PHP_EOL;
    }
    $text .= $output;
    file_put_contents($fileName, $text, LOCK_EX);
} else {
    $text = '';
    if ($start) {
        $text .= PHP_EOL . '/////// start ///////' . PHP_EOL . $date . PHP_EOL . '/////// start ///////' . PHP_EOL;
    }
    $text .= $output;
    file_put_contents($fileName, $text, FILE_APPEND | LOCK_EX);
}

echo json_encode($data);
