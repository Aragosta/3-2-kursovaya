// конфигурация чат-бота для неавторизованного пользователя
const configChatbot = {};
// CSS-селектор кнопки, посредством которой будем вызывать окно диалога с чат-ботом
configChatbot.btn = '.chatbot__btn';
// ключ для хранения отпечатка браузера
configChatbot.key = 'fingerprint';
// реплики чат-бота
configChatbot.replicas = {
    bot: {
        0: {
            content: ['Привет!', 'Я Арагоста - чат-бот этого сайта'],
            human: [0, 1, 2]
        },
        1: {
            content: 'Я тоже рад, как мне к Вам обращаться?',
            human: [3]
        },
        2: {
            content: 'Как мне к Вам обращаться?',
            human: [3]
        },
        3: {
            content: '{{name}}, что Вас интересует?',
            human: [4]
        },
        4: {
            content: '{{name}}, для этого перейдите в <a href="index.php" target="_blank">каталог товаров</a>, откройте карточку понравившегося товара и запомните его идентификатор. Затем, если вы авторизованы, выберете в чат-боте опцию «Хочу оформить заказ».',
            human: [6]
        }
    },
    human: {
        0: {
            content: 'Привет! Я рад с тобой познакомиться',
            bot: 1
        },
        1: {
            content: 'Салют!',
            bot: 2
        },
        2: {
            content: 'Здравствуйте',
            bot: 2
        },
        3: {
            content: '',
            bot: 3,
            name: 'name'
        },
        4: {
            content: 'Меня интересует, как я могу оформить заказ',
            bot: 4
        },
        6: {
            content: 'В начало',
            bot: 3
        }
    }
}
// корневой элемент
configChatbot.root = SimpleChatbot.createTemplate();
// URL chatbot.php
configChatbot.url = '../static/js/chatbot/chatbot.php';
// создание SimpleChatbot
let chatbot = new SimpleChatbot(configChatbot);
// при клике по кнопке configChatbot.btn
document.querySelector(configChatbot.btn).onclick = function (e) {
    this.classList.add('d-none');
    const $tooltip = this.querySelector('.chatbot__tooltip');
    if ($tooltip) {
        $tooltip.classList.add('d-none');
    }
    configChatbot.root.classList.toggle('chatbot_hidden');
    chatbot.init();
};

// добавление ключа для хранения отпечатка браузера в LocalStorage
let fingerprint = localStorage.getItem(configChatbot.key);
if (!fingerprint) {
    Fingerprint2.get(function (components) {
        fingerprint = Fingerprint2.x64hash128(components.map(function (pair) {
            return pair.value
        }).join(), 31)
        localStorage.setItem(configChatbot.key, fingerprint)
    });
}

// подсказка для кнопки
const $btn = document.querySelector(configChatbot.btn);
$btn.addEventListener('mouseover', function (e) {
    const $tooltip = $btn.querySelector('.chatbot__tooltip');
    if (!$tooltip.classList.contains('chatbot__tooltip_show')) {
        $tooltip.classList.remove('d-none');
        setTimeout(function () {
            $tooltip.classList.add('chatbot__tooltip_show');
        }, 0);
    }
});
$btn.addEventListener('mouseout', function (e) {
    const $tooltip = $btn.querySelector('.chatbot__tooltip');
    if ($tooltip.classList.contains('chatbot__tooltip_show')) {
        $tooltip.classList.remove('chatbot__tooltip_show');
        setTimeout(function () {
            $tooltip.classList.add('d-none');
        }, 200);
    }
});

setTimeout(function () {
    const tooltip = document.querySelector('.chatbot__tooltip');
    tooltip.classList.add('chatbot__tooltip_show');
    setTimeout(function () {
        tooltip.classList.remove('chatbot__tooltip_show');
    }, 10000)
}, 10000);