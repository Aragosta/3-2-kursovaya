<?php
    // Открытие новой сессии
    session_start();

    // Очистка данных пользователя
    unset($_SESSION["email"]);
    unset($_SESSION["password"]);

    // Возвращение на страницу, на которой была нажата кнопка выхода
    header("HTTP/1.1 301 Moved Permanently");
    header("Location: /index.php");
?>