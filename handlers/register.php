<?php
// Запускаем сессию
session_start();
// Добавляем файл подключения к БД
require_once("dbconnect.php");
// Объявляем ячейку для добавления ошибок, которые могут возникнуть при обработке формы.
$_SESSION["error_messages"] = '';
// Объявляем ячейку для добавления успешных сообщений
$_SESSION["success_messages"] = '';

$_SESSION["returnedName"] = '';
$_SESSION["returnedSurname"] = '';
$_SESSION["returnedPhone"] = '';
$_SESSION["returnedEmail"] = '';

// Валидация на сервере

// Проверяем была ли отправлена форма, то есть была ли нажата кнопка зарегистрироваться. Если да, то идём дальше,
// если нет, значит пользователь зашёл на эту страницу напрямую. В этом случае выводим ему сообщение об ошибке.
if (isset($_POST["btn_submit_register"]) && !empty($_POST["btn_submit_register"])) {
    // Проверяем, есть ли в глобальном массиве $_POST данные, отправленные из формы,
    // и заключаем переданные данные в обычные переменные.

    // ПРОВЕРКА ИМЕНИ
    if (isset($_POST["first_name"])) {
        // Обрезаем пробелы с начала и с конца строки
        $first_name = trim($_POST["first_name"]);

        // Проверяем переменную на пустоту
        if (!empty($first_name)) {
            // Для безопасности, преобразуем специальные символы в HTML-сущности
            $first_name = htmlspecialchars($first_name, ENT_QUOTES);
        } else {
            // Сохраняем в сессию сообщение об ошибке.
            $_SESSION["error_messages"] .= "<p class='mesage_error'>Укажите Ваше имя</p>";
            $_SESSION["returnedName"] = $first_name;
            // Возвращаем пользователя на страницу регистрации
            header("HTTP/1.1 301 Moved Permanently");
            header("Location: " . $address_site . "/elements/form_register.php");
            // Останавливаем скрипт
            exit();
        }
    } else {
        // Сохраняем в сессию сообщение об ошибке.
        $_SESSION["error_messages"] .= "<p class='mesage_error'>Отсутствует поле с именем</p>";
        // Возвращаем пользователя на страницу регистрации
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: " . $address_site . "/elements/form_register.php");
        // Останавливаем скрипт
        exit();
    }

    // ПРОВЕРКА ФАМИЛИИ
    if (isset($_POST["last_name"])) {
        // Обрезаем пробелы с начала и с конца строки
        $last_name = trim($_POST["last_name"]);

        if (!empty($last_name)) {
            // Для безопасности, преобразуем специальные символы в HTML-сущности
            $last_name = htmlspecialchars($last_name, ENT_QUOTES);
        } else {
            // Сохраняем в сессию сообщение об ошибке.
            $_SESSION["error_messages"] .= "<p class='mesage_error'>Укажите Вашу фамилию</p>";
            $_SESSION["returnedSurname"] = $last_name;
                // Возвращаем пользователя на страницу регистрации
            header("HTTP/1.1 301 Moved Permanently");
            header("Location: " . $address_site . "/elements/form_register.php");
            // Останавливаем скрипт
            exit();
        }
    } else {
        // Сохраняем в сессию сообщение об ошибке.
        $_SESSION["error_messages"] .= "<p class='mesage_error'>Отсутствует поле с фамилией</p>";
        // Возвращаем пользователя на страницу регистрации
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: " . $address_site . "/elements/form_register.php");
        // Останавливаем скрипт
        exit();
    }

    // ПРОВЕРКА ТЕЛЕФОНА
    if (isset($_POST["phone"])) {
        // Обрезаем пробелы с начала и с конца строки
        $phone = trim($_POST["phone"]);

        if (!empty($phone)) {
            // Для безопасности, преобразуем специальные символы в HTML-сущности
            $phone = htmlspecialchars($phone, ENT_QUOTES);
        } else {
            // Сохраняем в сессию сообщение об ошибке.
            $_SESSION["error_messages"] .= "<p class='mesage_error'>Укажите Ваш номер телефона</p>";
            $_SESSION["returnedPhone"] = $phone;
            // Возвращаем пользователя на страницу регистрации
            header("HTTP/1.1 301 Moved Permanently");
            header("Location: " . $address_site . "/elements/form_register.php");
            // Останавливаем скрипт
            exit();
        }
    } else {
        // Сохраняем в сессию сообщение об ошибке.
        $_SESSION["error_messages"] .= "<p class='mesage_error'>Отсутствует поле с телефоном</p>";
        // Возвращаем пользователя на страницу регистрации
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: " . $address_site . "/elements/form_register.php");
        // Останавливаем скрипт
        exit();
    }

    // ПРОВЕРКА EMAIL
    if (isset($_POST["email"])) {
        // Обрезаем пробелы с начала и с конца строки
        $email = trim($_POST["email"]);

        if (!empty($email)) {
            // Для безопасности, преобразуем специальные символы в HTML-сущности
            $email = htmlspecialchars($email, ENT_QUOTES);
            // Проверяем формат полученного почтового адреса с помощью регулярного выражения
            $reg_email = "/^[a-z0-9][a-z0-9\._-]*[a-z0-9]*@([a-z0-9]+([a-z0-9-]*[a-z0-9]+)*\.)+[a-z]+/i";

            // Если формат полученного почтового адреса не соответствует регулярному выражению
            if (!preg_match($reg_email, $email)) {
                // Сохраняем в сессию сообщение об ошибке.
                $_SESSION["error_messages"] .= "<p class='mesage_error' >Вы ввели неправильный email</p>";
                $_SESSION["returnedEmail"] = $email;
                // Возвращаем пользователя на страницу регистрации
                header("HTTP/1.1 301 Moved Permanently");
                header("Location: " . $address_site . "/elements/form_register.php");
                // Останавливаем скрипт
                exit();
            }
            // Проверяем, нет ли уже такого адреса в БД
            $result_query = $mysqli->query("SELECT `email` FROM `users` WHERE `email`='" . $email . "'");

            // Если кол-во полученных строк ровно единице, значит пользователь с таким почтовым адресом уже зарегистрирован
            if ($result_query->num_rows == 1) {
                // Если полученный результат не равен false
                if (($row = $result_query->fetch_assoc())) {
                    // Сохраняем в сессию сообщение об ошибке.
                    $_SESSION["error_messages"] .= "<p class='mesage_error' >Пользователь с таким почтовым адресом уже зарегистрирован</p>";
                    $_SESSION["returnedName"] = $first_name;
                    $_SESSION["returnedSurname"] = $last_name;
                    $_SESSION["returnedPhone"] = $phone;
                    $_SESSION["returnedEmail"] = $email;
                } else {
                    // Сохраняем в сессию сообщение об ошибке.
                    $_SESSION["error_messages"] .= "<p class='mesage_error' >Ошибка в запросе к БД</p>";
                    $_SESSION["returnedEmail"] = $email;
                }
                // Возвращаем пользователя на страницу регистрации
                header("HTTP/1.1 301 Moved Permanently");
                header("Location: " . $address_site . "/elements/form_register.php");
                // Закрытие выборки
                $result_query->close();
                // Останавливаем скрипт
                exit();
            }
            // закрытие выборки
            $result_query->close();
        } else {
            // Сохраняем в сессию сообщение об ошибке.
            $_SESSION["error_messages"] .= "<p class='mesage_error'>Укажите Ваш email</p>";
            // Возвращаем пользователя на страницу регистрации
            header("HTTP/1.1 301 Moved Permanently");
            header("Location: " . $address_site . "/elements/form_register.php");
            // Останавливаем скрипт
            exit();
        }
    } else {
        // Сохраняем в сессию сообщение об ошибке.
        $_SESSION["error_messages"] .= "<p class='mesage_error'>Отсутствует поле для ввода Email</p>";
        // Возвращаем пользователя на страницу регистрации
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: " . $address_site . "/elements/form_register.php");
        // Останавливаем скрипт
        exit();
    }

    // ПРОВЕРКА ПАРОЛЯ
    if (isset($_POST["password"])) {
        // Обрезаем пробелы с начала и с конца строки
        $password = trim($_POST["password"]);

        if (!empty($password)) {
            // Для безопасности, преобразуем специальные символы в HTML-сущности
            $pasword = htmlspecialchars($password, ENT_QUOTES);
            // Шифруем пароль
            $hash = password_hash($_POST["password"], PASSWORD_BCRYPT);
        } else {
            // Сохраняем в сессию сообщение об ошибке.
            $_SESSION["error_messages"] .= "<p class='mesage_error'>Укажите Ваш пароль</p>";
            // Возвращаем пользователя на страницу регистрации
            header("HTTP/1.1 301 Moved Permanently");
            header("Location: " . $address_site . "/elements/form_register.php");
            // Останавливаем  скрипт
            exit();
        }
    } else {
        // Сохраняем в сессию сообщение об ошибке.
        $_SESSION["error_messages"] .= "<p class='mesage_error'>Отсутствует поле для ввода пароля</p>";
        // Возвращаем пользователя на страницу регистрации
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: " . $address_site . "/elements/form_register.php");
        // Останавливаем  скрипт
        exit();
    }
    // Запрос на добавление пользователя в БД
    $result_query_insert = $mysqli->query("INSERT INTO `users` (name, surname, email, password, phone) VALUES ('" . $first_name . "', '" . $last_name . "', '" . $email . "', '" . $hash . "', '" . $phone . "') ");

    if (!$result_query_insert) {
        // Сохраняем в сессию сообщение об ошибке.
        $_SESSION["error_messages"] .= "<p class='mesage_error'>Ошибка запроса на добавление пользователя в БД</p>";

        $_SESSION["returnedName"] = $first_name;
        $_SESSION["returnedSurname"] = $last_name;
        $_SESSION["returnedPhone"] = $phone;
        $_SESSION["returnedEmail"] = $email;

        // Возвращаем пользователя на страницу регистрации
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: " . $address_site . "/elements/form_register.php");
        // Останавливаем  скрипт
        exit();
    } else {
        $_SESSION["success_messages"] = "<p class='success_message'>Вы успешно зарегистрировались</p>";
        // Отправляем пользователя на страницу авторизации
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: " . $address_site . "/elements/form_auth.php");
    }
    // Завершение запроса
    $result_query_insert->close();
    // Закрываем подключение к БД
    $mysqli->close();
} else {
    exit("<p><strong>Ошибка!</strong> Вы зашли на эту страницу напрямую, поэтому нет данных для обработки. Вы можете перейти на <a href=" . $address_site . "> главную страницу </a>.</p>");
}
