<?php
// Запускаем сессию
session_start();
// Добавляем файл подключения к БД
require_once("dbconnect.php");
// Объявляем ячейку для добавления ошибок, которые могут возникнуть при обработке формы.
$_SESSION["error_messages"] = '';
// Объявляем ячейку для добавления успешных сообщений
$_SESSION["success_messages"] = '';

// Валидация на сервере

// Проверяем была ли отправлена форма, то есть была ли нажата кнопка зарегистрироваться. Если да, то идём дальше,
// если нет, значит пользователь зашёл на эту страницу напрямую. В этом случае выводим ему сообщение об ошибке.
if (isset($_POST["btn_submit_register"]) && !empty($_POST["btn_submit_register"])) {
    // Проверяем, есть ли в глобальном массиве $_POST данные, отправленные из формы,
    // и заключаем переданные данные в обычные переменные.

    // ПРОВЕРКА ИМЕНИ
    if (isset($_POST["first_name"])) {
        // Обрезаем пробелы с начала и с конца строки
        $first_name = trim($_POST["first_name"]);

        // Проверяем переменную на пустоту
        if (!empty($first_name)) {
            // Для безопасности, преобразуем специальные символы в HTML-сущности
            $first_name = htmlspecialchars($first_name, ENT_QUOTES);
        } else {
            // Сохраняем в сессию сообщение об ошибке.
            $_SESSION["error_messages"] .= "<p class='mesage_error'>Укажите Ваше имя</p>";
            // Возвращаем пользователя на страницу регистрации
            header("HTTP/1.1 301 Moved Permanently");
            header("Location: " . $address_site . "/home.php");
            // Останавливаем скрипт
            exit();
        }
    } else {
        // Сохраняем в сессию сообщение об ошибке.
        $_SESSION["error_messages"] .= "<p class='mesage_error'>Отсутствует поле с именем</p>";
        // Возвращаем пользователя на страницу регистрации
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: " . $address_site . "/home.php");
        // Останавливаем скрипт
        exit();
    }

    // ПРОВЕРКА ФАМИЛИИ
    if (isset($_POST["last_name"])) {
        // Обрезаем пробелы с начала и с конца строки
        $last_name = trim($_POST["last_name"]);

        if (!empty($last_name)) {
            // Для безопасности, преобразуем специальные символы в HTML-сущности
            $last_name = htmlspecialchars($last_name, ENT_QUOTES);
        } else {
            // Сохраняем в сессию сообщение об ошибке.
            $_SESSION["error_messages"] .= "<p class='mesage_error'>Укажите Вашу фамилию</p>";
            // Возвращаем пользователя на страницу регистрации
            header("HTTP/1.1 301 Moved Permanently");
            header("Location: " . $address_site . "/home.php");
            // Останавливаем скрипт
            exit();
        }
    } else {
        // Сохраняем в сессию сообщение об ошибке.
        $_SESSION["error_messages"] .= "<p class='mesage_error'>Отсутствует поле с фамилией</p>";
        // Возвращаем пользователя на страницу регистрации
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: " . $address_site . "/home.php");
        // Останавливаем скрипт
        exit();
    }

    // ПРОВЕРКА ПАРОЛЯ
    if (isset($_POST["password"])) {
        // Обрезаем пробелы с начала и с конца строки
        $password = trim($_POST["password"]);

        if (!empty($password)) {
            // Для безопасности, преобразуем специальные символы в HTML-сущности
            $pasword = htmlspecialchars($password, ENT_QUOTES);
            // Шифруем папроль
            $hash = password_hash($_POST["password"], PASSWORD_BCRYPT);
        } else {
            // Сохраняем в сессию сообщение об ошибке.
            $_SESSION["error_messages"] .= "<p class='mesage_error'>Укажите Ваш пароль</p>";
            // Возвращаем пользователя на страницу регистрации
            header("HTTP/1.1 301 Moved Permanently");
            header("Location: " . $address_site . "/home.php");
            // Останавливаем скрипт
            exit();
        }
    } else {
        // Сохраняем в сессию сообщение об ошибке.
        $_SESSION["error_messages"] .= "<p class='mesage_error'>Отсутствует поле для ввода пароля</p>";
        // Возвращаем пользователя на страницу регистрации
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: " . $address_site . "/home.php");
        // Останавливаем скрипт
        exit();
    }
    // Запрос на обновление пользователя в БД
    $email = $_SESSION["email"];
    $result_query_insert = $mysqli->query("UPDATE `users` SET name = '" . $first_name . "', surname = '" . $last_name . "', password = '" . $hash . "' WHERE email = '" . $email . "'");

    if (!$result_query_insert) {
        // Сохраняем в сессию сообщение об ошибке.
        $_SESSION["error_messages"] .= "<p class='mesage_error'>Ошибка запроса на добавление пользователя в БД</p>";
        // Возвращаем пользователя на страницу регистрации
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: " . $address_site . "/home.php");
        // Останавливаем скрипт
        exit();
    } else {
        $_SESSION["success_messages"] = "<p class='success_message'>Смена данных прошла успешно</p>";
        $_SESSION['login'] = $first_name . " " . $last_name;
            // Отправляем пользователя на страницу авторизации
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: " . $address_site . "/home.php");
    }
    // Завершение запроса
    $result_query_insert->close();
    // Закрываем подключение к БД
    $mysqli->close();
} else {
    exit("<p><strong>Ошибка!</strong> Вы зашли на эту страницу напрямую, поэтому нет данных для отображения. Вы можете перейти на <a href=" . $address_site . "> главную страницу </a>.</p>");
}
