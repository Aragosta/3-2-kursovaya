<?php
//Подключение шапки
require_once("elements/base/header.php");
?>
    <!-- Подключение Bootstrap CSS -->
    <link rel="stylesheet" href="../bootstrap-5.0.1-dist/bootstrap-5.0.1-dist/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../static/css/styles.css">
    <!-- Блок для вывода сообщений -->
    <div class="block_for_messages">
        <?php
        // Если в сессии существуют сообщения об ошибках, то выводим их
        if (isset($_SESSION["error_messages"]) && !empty($_SESSION["error_messages"])) {
            echo $_SESSION["error_messages"];

            // Уничтожаем чтобы не выводились заново при обновлении страницы
            unset($_SESSION["error_messages"]);
        }

        // Если в сессии существуют радостные сообщения, то выводим их
        if (isset($_SESSION["success_messages"]) && !empty($_SESSION["success_messages"])) {
            echo $_SESSION["success_messages"];

            // Уничтожаем чтобы не выводились заново при обновлении страницы
            unset($_SESSION["success_messages"]);
        }
        ?>
    </div>

<?php
// Проверяем, если пользователь не авторизован, то выводим форму регистрации,
// иначе выводим сообщение о том, что он уже зарегистрирован
if (isset($_SESSION["email"]) && isset($_SESSION["password"])) {
    $email = $_SESSION['email'];

    $server = "localhost";
    $username = "root";
    $password = "";
    $database = "web-kursovaya";

    // Подключение к базе данный
    $mysqli = new mysqli($server, $username, $password, $database);

    $result_query_select = $mysqli->query("SELECT * FROM `users` WHERE email = '" . $email . "'");
    $role = 1;
    foreach ($result_query_select as $row) {
        $role = $row["role"];
    }
    ?>
    <h2 class="text-center">Здравствуйте, <?= $_SESSION['login'] ?></h2>
    <!--<h2>Здравствуйте, Пользователь</h2>-->
    <h3 class="text-center">Здесь Вы можете поменять свои данные авторизации</h3>
    <div id="form_register">
        <form action="../handlers/change_info.php" method="post" name="form_register">
            <!--Имя-->
            <div>
                <div class="input-group input-group-sm mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-sm">Имя</span>
                    </div>
                    <input type="text" class="form-control" name="first_name" aria-label="Small" maxlength="50" autocomplete="off" required="required" aria-describedby="inputGroup-sizing-sm">
                </div>
                <div><span id="valid_first_name_message" class="mesage_error mb-1"></span></div>
            </div>
            <!--Фамилия-->
            <div>
                <div class="input-group input-group-sm mt-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-sm">Фамилия</span>
                    </div>
                    <input type="text" class="form-control" name="last_name" aria-label="Small" maxlength="50" autocomplete="off" required="required" aria-describedby="inputGroup-sizing-sm">
                </div>
                <div><span id="valid_last_name_message" class="mesage_error mb-1"></span></div>
            </div>
            <!--Новый пароль-->
            <div>
                <div class="input-group input-group-sm mt-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-sm">Новый пароль</span>
                    </div>
                    <input type="password" class="form-control" name="password" placeholder="минимум 6 символов" aria-label="Small" maxlength="50" required="required" aria-describedby="inputGroup-sizing-sm">
                </div>
                <div><span id="valid_password_message" class="mesage_error mb-1"></span></div>
            </div>
            <!--Подтверждение пароля-->
            <div>
                <div class="input-group input-group-sm mt-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-sm">Подтверждение пароля</span>
                    </div>
                    <input type="password" class="form-control" name="password_check" placeholder="минимум 6 символов" aria-label="Small" maxlength="50" required="required" aria-describedby="inputGroup-sizing-sm">
                </div>
                <div><span id="valid_password_check_message" class="mesage_error mb-1"></span></div>
            </div>

            <input type="submit" name="btn_submit_register" class="btn btn-primary mt-3" style="margin: 0 auto;" value="Поменять данные">
        </form>
        <?php
        if ($role == 2) {
        ?>
        <div style="margin-top: 20px; display: flex; justify-content: space-between;">
            <a href="/orders.php">Просмотр заявок</a>
            <a href="/articles.php">Артикулы товаров</a>
        </div>
        <?php
        }
        ?>

        <!-- Подключение Bootstrap JavaScript с Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    </div>
    <?php
} else {
    ?>
    <div id="authorized">
        <h2>Авторизуйтесь для просмотра личного кабинета</h2>
    </div>
    <?php
}

// Подключение подвала
require_once("elements/base/footer.php");
?>