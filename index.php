<?php
// Подключение шапки страницы
require_once("elements/base/header.php");
?>
<link href="buzina-pagination/buzina-pagination.min.css" rel="stylesheet">
<div id="content" style="margin-top: 20px;">
    <div class="container goods">
        <div id="pag" style="min-height: 480px">
            <?php
                // данные подключения к базе данных
                $server = "localhost";
                $username = "root";
                $password = "";
                $database = "web-kursovaya";

                // Подключение к базе данный
                $mysqli = new mysqli($server, $username, $password, $database);

                // Проверка успешности соединения
                if ($mysqli->connect_errno) {
                    die("<p><strong>Ошибка подключения к БД</strong></p><p><strong>Код ошибки: </strong> " . $mysqli->connect_errno . " </p><p><strong>Описание ошибки:</strong> " . $mysqli->connect_error . "</p>");
                }

                // Устанавливаем кодировку подключения
                $mysqli->set_charset('utf8');

                // получаем список товаров
                $query_all_products = $mysqli->query("SELECT * FROM `products`");

                $count_products = 0;

                foreach ($query_all_products as $row_pr) {
                    $count_products += 1;
                    echo '<div class="block text-center" data-bs-toggle="modal" data-bs-target="#modal' . $count_products . '">';

                    $query_string = "SELECT link_to_image FROM `pictures` join `products_pictures` WHERE `products_pictures`.`id_picture` = `pictures`.`id_picture` and `products_pictures`.`id_product` = " . $row_pr["id_product"] . " LIMIT 1";
                    $query_picture = $mysqli->query($query_string);

                    $pic = '';
                    foreach ($query_picture as $row_img) {
                        $pic = $row_img["link_to_image"];
                        echo '<img class="sld" src="' . $pic . '">';
                    }
                    echo $row_pr["name"];
                    echo '</div>';
                }
            ?>
        </div>
    </div>

    <!-- Модальные окна -->
    <div id="container-with-modals">
        <?php
        // данные подключения к базе данных
        $server = "localhost";
        $username = "root";
        $password = "";
        $database = "web-kursovaya";

        // Подключение к базе данный
        $mysqli = new mysqli($server, $username, $password, $database);

        // Проверка успешности соединения
        if ($mysqli->connect_errno) {
            die("<p><strong>Ошибка подключения к БД</strong></p><p><strong>Код ошибки: </strong> " . $mysqli->connect_errno . " </p><p><strong>Описание ошибки:</strong> " . $mysqli->connect_error . "</p>");
        }

        // Устанавливаем кодировку подключения
        $mysqli->set_charset('utf8');

        // получаем список товаров
        $query_all_products = $mysqli->query("SELECT * FROM `products`");

        $count_products = 0;

        foreach ($query_all_products as $row_pr) {
            $count_products += 1;

            $query_string = "SELECT link_to_image FROM `pictures` join `products_pictures` WHERE `products_pictures`.`id_picture` = `pictures`.`id_picture` and `products_pictures`.`id_product` = " . $row_pr["id_product"] . " LIMIT 1";
            $query_picture = $mysqli->query($query_string);

            $pic = '';
            foreach ($query_picture as $row_img) {
                $pic = $row_img["link_to_image"];
            }

            // Модальное окно
            echo '<div class="modal fade" id="modal' . $count_products . '" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">';
            echo '<div class="modal-dialog">';
            echo '<div class="modal-content">';

            echo '<div class="modal-header">';
            echo '<h5 class="modal-title item_title" id="exampleModalLabel">' . $row_pr["name"] . '</h5>';
            echo '<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>';
            echo '</div>';

            echo '<div class="modal-body text-center">';
            echo '<img class="sld-modal" src="' . $pic . '" alt="0"> Цена: <span class="item_price">от ' . $row_pr["cost_min"] . '</span>₽';
            echo '</div>';

            echo '<div class="modal-header">';
            echo '<h5 class="modal-title item_title" id="exampleModalLabel">Описание</h5>';
            echo '</div>';

            echo '<div class="modal-body">';
            echo $row_pr["description"];
            echo '</div>';

            echo '<div class="modal-header">';
            echo '<h5 class="modal-title item_title" id="exampleModalLabel">Идентификатор товара</h5>';
            echo '</div>';

            echo '<div class="modal-body">';
            echo $row_pr["id_product"];
            echo '</div>';

            echo '<div class="modal-footer">Контактные данные: 8 800 555-35-35</div>';

            echo '</div>';
            echo '</div>';
            echo '</div>';
        }
        ?>
    </div>
    <!-- chatbot__btn -->
    <div class="chatbot__btn">
        <div class="chatbot__tooltip d-none">Есть вопрос?</div>
    </div>
    <!-- FingerPrint JS -->
    <script src="static/js/chatbot/fp2.js"></script>
    <!-- ChatBot JS -->
    <script src="static/js/chatbot/chatbot.js"></script>

    <!-- Подключение Bootstrap JavaScript с Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="static/js/pagination.js"></script>
    <script src="buzina-pagination/buzina-pagination.min.js"></script>
</div>
<?php
// Проверка авторизованности пользователя
// Пользователь не вошел
if (!isset($_SESSION['email']) && !isset($_SESSION['password'])) {
?>
<!-- Конфигурация -->
<script src="static/js/chatbot/configuration_guest.js"></script>
<?php
} else {
    // Пользователь вошел
?>
<!-- Конфигурация -->
<script src="static/js/chatbot/configuration.js"></script>
<?php
}
?>
<?php
//Подключение подвала
require_once("elements/base/footer.php");
?>